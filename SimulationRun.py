from MBOBase import *
import numpy as np

class SimulationRun():
    #int numAgents:             number of agents
    #int width:                 width of grid
    #int height:                height of grid
    #int extraSteps:            number of additional steps to run (used for reselling)
    #boolean agentSelling:      allows agents to resell their grid spaces
    #int rewardScale:           scales the reward for exploring a grid space
    #string sellOrder:          controls the order in which grid spaces are sold (options are: "random", "sequential", "shortest")
    #boolean randomCost:        randomizes the cost grid
    #float removeEdgePercent:   removes a percentage of the edges.
    @staticmethod
    def run(numAgents=3, width=5, height=5, extraSteps=0, agentSelling=False, rewardScale=100, sellOrder="random", randomCost=False, randomCostMin = 0, randomCostMax = 10, removeEdgePercent=0.0):
        steps = width * height - numAgents + 20 + extraSteps
        model = SearchModel(numAgents, width, height, "placeall", 0, 0, steps, sellOrder)
        model.allowAgentSelling = agentSelling
        model.rewardScale = rewardScale
        #model.sellingOrder = sellOrder
        if randomCost:
            model.costGrid = SearchDirectedGrid(model, 0, True, randomCostMin, randomCostMax)
        elif not randomCost:
            model.costGrid = SearchDirectedGrid(model, 0, False, 0, 10)

        if not removeEdgePercent == 0.0:
            model.removePercentOfPaths(removeEdgePercent)

        for i in range(steps):
            model.step()

        search_times = []

        for agent in model.schedule.agents:
            agent_search_time = 0
            for node in agent.ownedList:
                agent_search_time += model.costGrid.node_costs[(node[0], node[1])]

            search_times.append(agent_search_time)

        max_time = max(search_times)
        min_time = min(search_times)
        avg_time = np.mean(search_times)
        std_time = np.std(search_times)

        #print("Max Search Time: ", str(max_time))
        #print("Min Search Time: ", str(min_time))
        #print("Avg Search Time: ", str(avg_time))
        #print("Std Search Time: ", str(std_time))

        return (max_time, min_time, avg_time, std_time)

# SimulationRun.run(3, 5, 5, 0, False, 100, "shortest")

# numAgents = 3
# width = 5
# height = 5
# steps = width*height-numAgents+40
#
#
# model = SearchModel(numAgents, width, height, "placeall", 2, 2, steps, "shortest")
#
# i = 0
# for i in range(steps):
#     model.step()

#totalOwned = 0
#for a in model.schedule.agents:
#     print("\n")
#     print("Agent "+str(a.selfId)+"'s starting space: "+str(a.xstart)+", "+str(a.ystart))
#     print("Agent "+str(a.selfId)+"'s path length: "+str(a.pathLength))
#     print("Agent " + str(a.selfId) + "'s owned spaces: " + str(a.ownedList))
#     print("Agent "+str(a.selfId)+"'s path: "+str(a.path))
#     totalOwned+=len(a.ownedList)
#
# print("Total number of spaces owned: "+str(totalOwned)+" (Numbers higher than Height*Width indicate that multiple agents started on the same location)")
#print("Items remaining in auctioneer's selling list: "+str(model.auctioneer.sellingList))