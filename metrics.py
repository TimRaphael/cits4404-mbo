import matplotlib.pyplot as plt
import numpy as np

from MBOBase import *

<<<<<<< Updated upstream
numAgents = 3
width = 6
height = 6
steps = width*height-numAgents+20

model = SearchModel(numAgents, width,width, "placeall", 0, 0, steps, "random")
model.allowAgentSelling = True
=======
numAgents = 20
width = 15
height = 15
steps = width*height-numAgents+40

model = SearchModel(numAgents, width,width, "placeall", 0, 0, steps, "sequential")
>>>>>>> Stashed changes

def run_sim():
	#model.removePercentOfPaths(0.10)
	for i in range(steps):
		model.step()

def gen_hist_path_length():
	run_sim()
	agent_path_lengths = [a.pathLength for a in model.schedule.agents]

	plt.hist(agent_path_lengths)
	plt.ylabel('Number of Agents')
	plt.xlabel('Path Length')
	plt.yticks(range(0, numAgents, 1))
	plt.xticks(range(0, max(agent_path_lengths), 20))
	plt.show()

def gen_path_times():
	run_sim()
	search_times = []

	for agent in model.schedule.agents:
		agent_search_time = 0
		for node in agent.ownedList:
			agent_search_time += model.costGrid.node_costs[(node[0], node[1])]

		search_times.append(agent_search_time)


	max_time = max(search_times)
	min_time = min(search_times)
	avg_time = np.mean(search_times)
	std_time = np.std(search_times)

	print("Max Search Time: ", str(max_time))
	print("Min Search Time: ", str(min_time))
	print("Avg Search Time: ", str(avg_time))
	print("Std Search Time: ", str(std_time))

	return (max_time, min_time, avg_time, std_time)

def gen_colour_map():
	import matplotlib.pyplot as plt
	import numpy as np

	#run_sim()

	ar = np.zeros([model.grid.width, model.grid.height])

	print("Agents: ", len(model.schedule.agents))

	for agent in model.schedule.agents:
		for node in agent.ownedList:
			ar[node[0], node[1]] = agent.selfId

	print(ar)

	#print(model.grid.grid)
	# for a in model.schedule.agents:
	# 	print(a.pos)

	# plt.subplot(2, 1, 1)
	# c = plt.pcolor(model.grid.grid)
	# plt.title('default: no edges')
	# plt.show()

def gen_suboptimal_sales():
	# Number of squares in an agents path that the agent doesn't own.
	numSquaresNotOwnedInPath = 0

	for a in model.schedule.agents:
		for sq in a.ownedList:
			if tuple(sq) not in a.path:
				numSquaresNotOwnedInPath += 1

	print("Number of sub-optimal sells: ", numSquaresNotOwnedInPath)

def print_paths():
	for a in model.schedule.agents:
		print("A: " + str(a.selfId) + " Path: " + str(a.path))

def print_ownedList():
	for a in model.schedule.agents:
		print("A: " + str(a.selfId) + " Owned List: " + str(a.ownedList))



if __name__ == '__main__':
	gen_hist_path_length()
	gen_path_times()
	gen_colour_map()
	gen_suboptimal_sales()
	print_paths()
	print_ownedList()