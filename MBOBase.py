from mesa import Agent, Model
from mesa.space import MultiGrid
from mesa.time import BaseScheduler
import TSP
import networkx as nx
import random
#import math


# Type of Agents
#   Search time (squares per step)
#   Travel time (squares per step)
#

class SearchModel(Model):
    """A model with some number of agents."""
    def __init__(self, N, width, height, placemode, startx, starty, steps, sellOrder):
        self.num_agents = N
        self.grid = MultiGrid(width, height, True)
        self.costGrid = SearchDirectedGrid(self, "Type 1", False, 0, 10) # Type currently ignored
        self.schedule = BaseScheduler(self)
        self.randomCostMin = 0
        self.randomCostMax = 10
        self.selling = True
        self.highestBid = [-width*height,-1]
        self.forSale = [-1,-1]
        self.startingPositions = []

        self.sellingOrder = sellOrder #Can be "random", "sequential", or "shortest"
        if not placemode == "placeall" and sellOrder == "shortest": #Default back to random selling order in the invalid case of random start + shortest selling order.
            self.sellingOrder = "random"
        self.steps = steps
        self.stepcount = 0

        self.allowAgentSelling = False #When True, allows agents to resell their owned spaces once the regular sale of all spaces is completed. Otherwise the model terminates after regular sales are completed.
        self.randomAgentSellingOrder = False #When True, the agent picked to sell an owned space is selected at random. Otherwise they are chosen in order from first to last.
        self.sellRandomSpace = False #When True, the space selected to resell is chosen at random, otherwise the worst space is chose. (Computationally expensive when False, especially if TSPOpt below is False)

        self.TSPOpt = True # If true, uses a simplified/optimised TSP solver, if false, uses brute-force TSP solver.
        self.rewardScale = 500  # Scales the reward between this value and 0. This should can be adjusted based on how much we value a shorter path or how much we value an even distribution. Higher scales encourage even distributions. Lower scales encourage shorter paths.
        # Create agents
        for i in range(self.num_agents):
            # Add the agents to the grid.
            if placemode=="random":
                #All agents start at randomised locations.
                x = random.randrange(self.grid.width)
                y = random.randrange(self.grid.height)
            elif placemode=="placeall":
                x = startx
                y = starty
                #All agents start at a the same specified location.
                if startx > width:
                    print('Starting x position should be no greater than the width of the grid. Starting at maximum width.')
                    x = width
                if starty > height:
                    print('Starting y position should be no greater than the height of the grid. Starting at maximum height.')
                    y = height
            a = MarketAgent(i, self, "Type 1", 1, 1,x, y)
            print("Agent "+str(i)+" starting at "+str(x)+", "+str(y))
            self.startingPositions.append([x,y])
            self.schedule.add(a)
            self.grid.place_agent(a, (x, y))
        a = Auctioneer(N + 1, self, width, height)
        self.auctioneer = a
                    
    def step(self):
        self.auctioneer.step()
        self.schedule.step()
        self.stepcount += 1


    def removePercentOfPaths(self, percentFloat):
        edges = nx.edges(self.costGrid.grid)
        print("Num Edges: ", len(edges))

        for i in range(0, int(len(edges)*percentFloat)):
            to_remove = random.choice(edges)
            self.costGrid.grid.remove_edge(to_remove[0], to_remove[1])
            edges.remove(to_remove)

        print("New Num Edges: ", len(nx.edges(self.costGrid.grid)))

    def dumpCostGridToJSON(self, filename):
        try:
            nx.write_yaml(self.costGrid.grid, filename)
        except Exception as e:
            print(e.message, e.args)
            raise e

    def loadCostGridFromJSON(self, filename):
        try:
            self.costGrid.grid = nx.read_yaml(filename)
        except Exception as e:
            print(e.message, e.args)
            raise e


        
class Auctioneer(Agent):
    """An agent that does all of the auctioning."""
    def __init__(self, unique_id, model, width, height):
        super().__init__(unique_id, model)
        #Define starting parameters of the Auctioneer.
        self.width = width
        self.height = height
        #Start with all grid spaces up for sale.
        if not self.model.sellingOrder == "shortest":
            self.sellingList = [] #Populate the list sequentially, starting at 0,0.
            for i in range(width):
                for j in range(height):
                    self.sellingList.append([i,j])
            if self.model.sellingOrder == "random":
                random.shuffle(self.sellingList) #If random, simply shuffle the list.
        elif self.model.sellingOrder == "shortest":
            start = self.model.startingPositions[0]
            self.sellingList = []

            for dist in range(1, max(width, height)):
                #print(dist)
                #print(range(start[0]-dist, start[0]+dist))
                for x in range(start[0]-dist, start[0]+dist+1):
                    #print(x)
                    #print(start[0]+dist)
                    for y in range(start[1]-dist, start[1]+dist+1):
                        #print(start[1] + dist)
                        if x >= 0 and y >= 0 and x <= width-1 and y <= height-1:
                            if [x,y] not in self.sellingList:
                                self.sellingList.append([x,y])


        self.sellingOrder = 0
        #print(str(self.model.startingPositions))
        self.agentsSelling = False
        self.sellingList = [x for x in self.sellingList if x not in self.model.startingPositions]
        #print("Selling List: ", self.sellingList)
        #print(len(self.sellingList))
        #print(self.sellingList)
        #print("List of "+str(len(self.sellingList))+" spaces initially for sale: "+str(self.sellingList))

    # def insertSpace(self, uniqueID): #UNUSED, replaced by TSP Solver.
    #     newList = list(self.model.schedule.agents[uniqueID].ownedList)
    #     if len(self.model.schedule.agents[uniqueID].ownedList) < 2:  # If there is only 1 item in the list, no special inserting needs to be done.
    #         #print("Only one item in owned list, appending to end.")
    #         newList.append(self.model.forSale)
    #     else:  # If there are more, insert the new item such that the path length is minimized.
    #         bestDist = self.width * self.height
    #         bestI = 0
    #         inserted = False
    #         for i in range(len(self.model.schedule.agents[uniqueID].ownedList) - 1):
    #             # We always consider a pair of grid spaces.
    #             space1 = newList[i]
    #             space2 = newList[i + 1]
    #             # There are 3 possible inserts we can perform, before the first space, in between the two spaces, or after the second space.
    #             # First, if the new space is obviously in between the 2 spaces being considered, insert it there.
    #             if (space1[0] < self.model.forSale[0] < space2[0] and space1[1] < self.model.forSale[1] < space2[1]) or (space1[0] > self.model.forSale[0] > space2[0] and space1[1] > self.model.forSale[1] > space2[1]):
    #                 newList.insert(i + 1, self.model.forSale)
    #                 #print("Direct Insert")
    #                 inserted = True
    #                 break
    #             # Calculate the new space's distance to the 2 grid spaces being considered.
    #             dist1n = math.sqrt((space1[0] - self.model.forSale[0]) ** 2 + (space1[1] - self.model.forSale[1]) ** 2) # Distance from new space to first space
    #             dist2n = math.sqrt((space2[0] - self.model.forSale[0]) ** 2 + (space2[1] - self.model.forSale[1]) ** 2) # Distance from new space to second space
    #             dist12 = math.sqrt((space2[0] - space1[0]) ** 2 + (space2[1] - space1[1]) ** 2) # Distance from first space to second space
    #             #print("Agent "+str(uniqueID)+" has distances: 1-n: "+str(dist1n)+", 2-n: "+str(dist2n)+", 1-2: "+str(dist12))
    #             if (dist1n+dist12) < bestDist and i > 0: # New -> First -> Second, but can't insert before starting space.
    #                 # Insert before first space.
    #                 bestI = i
    #                 bestDist = dist1n + dist12
    #                 #print("New -> First -> Second, distance: "+str(bestDist))
    #             if (dist1n+dist2n) < bestDist: # First -> New -> Second
    #                 # Insert in between.
    #                 bestI = i+1
    #                 bestDist = dist1n + dist2n
    #                 #print("First -> New -> Second distance: "+str(bestDist))
    #             if (dist12+dist2n) < bestDist: # First -> Second -> New
    #                 # Insert after second space.
    #                 bestI = i+2
    #                 bestDist = dist2n + dist12
    #                 #print("First -> Second -> New distance: "+str(bestDist))
    #         if not inserted and not bestI == 0:
    #             newList.insert(bestI, self.model.forSale)
    #         elif not inserted: # If somehow none of the above catches the right insert, simply append.
    #             newList.append(self.model.forSale)
    #             #print("Didn't get caught")
    #     return newList



    def step(self):
        #Only take action if still in the Auctioning phase.
        if not self.sellingList:
            self.model.selling = False
        if not self.model.selling:
            pass
        
        #Check highest bidder from last round, award sale to their ID, remove it from the selling list.
        winnerID = self.model.highestBid[1]
        #print("Highest bid and ID: "+str(self.model.highestBid))
        if (not winnerID==-1) and (self.model.forSale in self.sellingList): #Only sells the space if a bid was actually placed.
            #self.model.schedule.agents[winnerID].ownedList = self.insertSpace(winnerID)
            self.model.schedule.agents[winnerID].ownedList.append(self.model.forSale)
            if self.model.TSPOpt and len(self.model.schedule.agents[winnerID].ownedList) > 0:
                self.model.schedule.agents[winnerID].ownedList = TSP.optimized_travelling_salesman(self.model.schedule.agents[winnerID].ownedList)
            elif len(self.model.schedule.agents[winnerID].ownedList) > 0:
                self.model.schedule.agents[winnerID].ownedList = TSP.travelling_salesman(self.model.schedule.agents[winnerID].ownedList)
            self.sellingList.remove(self.model.forSale)
            self.model.schedule.agents[winnerID].justBought = True
            self.model.grid.move_agent(self.model.schedule.agents[winnerID], self.model.forSale)
            print("Agent " + str(winnerID) + " just won space: " + str(self.model.forSale) + " with bid: "+str(self.model.highestBid[0]))
            #print("Agent " + str(winnerID) + "'s owned list: " + str(self.model.schedule.agents[winnerID].ownedList))
            print("\n")

        #Select a new space to sell, reinitialise the highest bid.
        if not len(self.sellingList)==0:
            self.model.forSale = self.sellingList[0]
            #self.model.forSale = random.choice(self.sellingList[:10]) # Random one of next 10 available squares
            #self.model.forSale = self.sellingList[0] # next square
            self.model.highestBid = [-self.width*self.height,-1]
        else:
            self.agentsSelling = True
            print("All grid spaces sold.")

        if self.agentsSelling and self.model.allowAgentSelling and self.model.stepcount < self.model.steps-1: # Pick an agent who will resell one of his nodes. Don't resell a node on the last step, since the purchase happens in the next step which won't happen.

            if self.model.randomAgentSellingOrder: #Pick a random agent and force them to sell a grid space.
                agentSelected = random.choice(self.model.schedule.agents)
                agentID = agentSelected.selfId

                if len(self.model.schedule.agents[agentID].ownedList) > 1 and self.model.sellRandomSpace: #Sell a random space from the list of owned spaces.
                    self.model.forSale = random.choice(self.model.schedule.agents[agentID].ownedList)
                    self.sellingList.append(self.model.forSale)
                    self.model.schedule.agents[agentID].ownedList.remove(self.model.forSale) #Remove the space being sold from the agent's owned list.
                    self.model.schedule.agents[agentID].justBought = True #Set this to true so that the agent recalculates its own path, since it just lost a node.

                elif len(self.model.schedule.agents[agentID].ownedList) > 1 and not self.model.sellRandomSpace: #Sell the worst space from the list of owned spaces.
                    self.model.forSale = self.model.schedule.agents[agentID].calculateWorstNode() #Find the worst space that this agent owns, put it up for sale.
                    if not self.model.forSale == [-1, -1]:
                        self.sellingList.append(self.model.forSale)
                        self.model.schedule.agents[agentID].ownedList.remove(self.model.forSale)  # Remove the space being sold from the agent's owned list.
                        self.model.schedule.agents[agentID].justBought = True  # Set this to true so that the agent recalculates its own path, since it just lost a node.

            elif not self.model.randomAgentSellingOrder: #Pick the next agent and force them to sell a grid space.
                # if self.sellingOrder >= self.model.num_agents:
                #     self.sellingOrder = 0
                # agentSelected = self.model.schedule.agents[self.sellingOrder]
                # agentID = agentSelected.selfId
                # self.sellingOrder += 1

                # Select the agents with the longest path first to sell
                agentSelected = None
                longestPath = 0

                for a in self.model.schedule.agents:
                    if not agentSelected:
                        agentSelected = a
                        longestPath = len(a.ownedList)
                    else:
                        if len(a.ownedList) > longestPath:
                            longestPath = len(a.ownedList)
                            agentSelected = a
                            print("Found a new longest path!")

                agentID = agentSelected.selfId


                if len(self.model.schedule.agents[agentID].ownedList) > 1 and self.model.sellRandomSpace: #Sell a random space from the list of owned spaces.
                    self.model.forSale = random.choice(self.model.schedule.agents[agentID].ownedList)
                    self.sellingList.append(self.model.forSale)
                    self.model.schedule.agents[agentID].ownedList.remove(self.model.forSale) #Remove the space being sold from the agent's owned list.
                    self.model.schedule.agents[agentID].justBought = True #Set this to true so that the agent recalculates its own path, since it just lost a node.

                elif len(self.model.schedule.agents[agentID].ownedList) > 1 and not self.model.sellRandomSpace: #Sell the worst space from the list of owned spaces.
                    self.model.forSale = self.model.schedule.agents[agentID].calculateWorstNode()  # Find the worst space that this agent owns, put it up for sale.
                    if not self.model.forSale == [-1,-1]:
                        self.sellingList.append(self.model.forSale)
                        self.model.schedule.agents[agentID].ownedList.remove(self.model.forSale)  # Remove the space being sold from the agent's owned list.
                        self.model.schedule.agents[agentID].justBought = True  # Set this to true so that the agent recalculates its own path, since it just lost a node.

            print("Agent "+str(agentSelected.selfId)+" selling grid space: "+str(self.model.forSale))


class MarketAgent(Agent):
    """ An agent with fixed initial wealth."""
    def __init__(self, unique_id, model, agentType, searchTime, travelTime, x, y):
        super().__init__(unique_id, model)
        #Define starting parameters of this type of agent.
        self.selfId = unique_id
        self.agentType = 0      #The agent type, 
        self.xstart = x         #Starting x coordinate.
        self.ystart = y         #Starting y coordinate.
        self.ownedList = []     #Stores the list of coordinates of grid spaces that this agent must visit.
        self.ownedList.append([x,y])
        self.justBought = False #Tells the agent if it succeeded the last round of bidding.
        self.path = []          #Stores the best path the agent will take to complete its task, as an ordered list of individual grid spaces to visit.
        self.path.append(tuple([x,y]))
        self.pathLength = 0     #Stores the path length of the best path.
        self.type = agentType
        self.searchTime = searchTime
        self.travelTime = travelTime

    def calculatePath(self, gridSpaces):
        #Calculates the best path for exploring all of the gridSpaces passed to it, starting from (xstart,ystart).

        resultPath = []

        #grid = nx.grid_2d_graph(self.model.grid.height,self.model.grid.width, create_using=nx.DiGraph())

        for i in range(0, len(gridSpaces)-1):
            # convert mesa gridspace into a NetworkX gridspace
            source_gs =   (gridSpaces[i][0],   gridSpaces[i][1])
            dest_gs   = (gridSpaces[i+1][0], gridSpaces[i+1][1])
            try:
                pathSegment = nx.dijkstra_path(self.model.costGrid.grid, source=source_gs, target=dest_gs, weight="cost")
                resultPath.extend(pathSegment[1:]) #remove the repeated start node
            except nx.exception.NetworkXNoPath as e:
                # The agent couldn't reach the next node
                print("Agent could not find path")
                return None

        return resultPath

    def calculatePathCost(self, gridSpaces):
        pathCost = 0 # total current search time
        
        for node in gridSpaces:
            pathCost += self.model.costGrid.node_costs[(node[0], node[1])]

        return pathCost

    def calculateWorstNode(self):
        bestCostDif = 0
        worstSpace = [-1, -1]
        newOwned1 = list(self.ownedList)
        newOwned2 = list(newOwned1)

        for space in newOwned1: #For every space owned, try making a path that doesn't include it. Remember the one which reduced the path length the most.
            newOwned2.remove(space)
            if self.model.TSPOpt: #Recalculate path, in case removing the node allows a new better path.
                newOwned2 = TSP.optimized_travelling_salesman(newOwned2)
            else:
                newOwned2 = TSP.travelling_salesman(newOwned2)
            newPath = self.calculatePath(newOwned2) #Calculate all intermediate nodes in path
            newLength = self.calculatePathCost(newPath) #Calculate new path cost
            costDif = self.pathLength-newLength #Calculate the improvement made
            if costDif>bestCostDif: #Compare this improvement against the saved value, if a better improvement is found, we have found a new worst grid space owned.
                bestCostDif=costDif
                worstSpace = space
            newOwned2 = list(newOwned1) #Restore the list such that for every iteration we have only ever removed one node from the list.

        print("Worst space: " + str(worstSpace) + " with Cost: " + str(bestCostDif))
        return worstSpace

    def calculateUtility(self, gridSpace):
        # Uses path length difference and rewards to calculate the net utility gain for buying a grid space.
        grid2 = tuple(gridSpace)
        if grid2 in self.path:
            cost = 0
            #return 0
            # print("Already on "+str(self.selfId)+"'s path")
        else:
            #newOwned = self.model.auctioneer.insertSpace(self.selfId)
            newOwned = list(self.ownedList)
            newOwned.append(gridSpace)
            if self.model.TSPOpt:
                newOwned = TSP.optimized_travelling_salesman(newOwned)
            else:
                newOwned = TSP.travelling_salesman(newOwned)
            result = self.calculatePath(newOwned)

            if result is not None:
                cost = self.calculatePathCost(result) - self.pathLength
            else:
                cost = 1000; # Some max cost
        
        ownedPercentage = (len(self.ownedList)/(self.model.grid.width*self.model.grid.height))*self.model.rewardScale #Percentage of all the grid spaces this agent already owns
        reward = self.model.rewardScale - ownedPercentage # Reward is a value between 100 and 0.
        # return random.randrange(10)
        # print("Agent "+str(self.selfId)+" bids: "+str(reward-cost)+" on space "+str(gridSpace)+", cost was: "+str(cost))
        return reward-cost
            

    def step(self):
        #Define what the agent does when its his turn.
        if self.model.selling and not self.model.forSale == [-1,-1]: #Perform normally
            if self.justBought: #If the agent was awarded a grid space last round, it must start by incorporating it into its path.
                result = self.calculatePath(self.ownedList) # None shouldn't be returned - still might happen?

                if result is None:
                    raise Exception("Too many edges removed, winning node has a non-path")

                self.path = result
                self.path.insert(0, tuple([self.xstart,self.ystart]))
                self.pathLength = self.calculatePathCost(self.path)
                self.justBought = False
            util = self.calculateUtility(self.model.forSale)   #Calculate the utility gain should the grid space be successfully bought.
            if util > self.model.highestBid[0]:                
                self.model.highestBid = [util, self.selfId]    #If the utility beats the best bid, replace it with this bid.

class SearchDirectedGrid:
    def __init__(self, _mesa_model, _agent_type, rand, min, max):
        self.grid = nx.grid_2d_graph(_mesa_model.grid.height, _mesa_model.grid.width, create_using=nx.DiGraph())
        self.node_costs = {}
        self.edge_costs = {}

        for node in nx.nodes(self.grid):
            if rand:
                #self.node_costs[node] = random.randrange(self.model.randomCostMin, self.model.randomCostMax)
                #self.node_costs[node] = random.randrange(10)
                self.node_costs[node] = random.randrange(min, max)
            else:
                self.node_costs[node] = 1

        nx.set_node_attributes(self.grid, "cost", self.node_costs)

        for edge in nx.edges(self.grid):
            self.edge_costs[edge] = self.node_costs[edge[1]] # assign the edge cost to that of the destination node.

        nx.set_edge_attributes(self.grid, "cost", self.edge_costs)
