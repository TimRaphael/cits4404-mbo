#!/usr/bin/env python

import concurrent.futures
import sys
from MBOBase import *
import numpy as np
import ast
from SimulationRun import *
import time

def run_task(task):
	return SimulationRun.run(**task["config"])


def gen_path_times(model):
	search_times = []

	for agent in model.schedule.agents:
		agent_search_time = 0
		for node in agent.ownedList:
			agent_search_time += model.costGrid.node_costs[(node[0], node[1])]

		search_times.append(agent_search_time)


	max_time = max(search_times)
	min_time = min(search_times)
	avg_time = np.mean(search_times)
	std_time = np.std(search_times)

	#print("Max Search Time: ", str(max_time))
	#print("Min Search Time: ", str(min_time))
	#print("Avg Search Time: ", str(avg_time))
	#print("Std Search Time: ", str(std_time))

	return (max_time, min_time, avg_time, std_time)

def main():
	experiments = []

	# open file as argument
	experiment_file = open(sys.argv[1])

	results_file = open("results_" + time.strftime("%Y%m%d%H%M%S") + ".csv", 'w')

	for line in experiment_file:
		# remove white space
		line = line.strip()
		
		# ignore comments
		if line[0] is '#':
			continue

		try:
			config = ast.literal_eval(line)
		except ValueError as e:
			raise e

		task = {'name' : config['name'], 'repeat' : int(config['repeat'])}
		del config['name']
		del config['repeat']

		task['config'] = config

		for r in range(0, task['repeat']):
			experiments.append(task.copy())


	# We can use a with statement to ensure threads are cleaned up promptly
	with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
	    # Start the load operations and mark each future with its URL
	    future_to_task = {executor.submit(run_task, t): t for t in experiments}
	    for future in concurrent.futures.as_completed(future_to_task):
	        task = future_to_task[future]
	        try:
	            results = future.result()
	        except Exception as exc:
	            #print('%r generated an exception: %s' % (num, exc))
	            raise exc
	        else:
	            print('Experiement: %s, Repeat: %d' % (task['name'], task['repeat']))


	            output = task['name'] + "," + str(task['config']['numAgents']) + "," + str(task['config']['width']) + "," + str(task['config']['height']) + "," + str(task['config']['extraSteps']) + "," + str(task['config']['agentSelling']) + "," + str(task['config']['rewardScale']) + "," + task['config']['sellOrder'] + "," + str(task['config']['randomCost']) + "," + str(task['config']['randomCostMin']) + "," + str(task['config']['randomCostMax']) + "," + str(task['config']['removeEdgePercent']) + "," + str(results[0]) + "," + str(results[1]) + "," + str(results[2]) + "," + str(results[3])
	            results_file.write(output + "\n")


if __name__ == "__main__":
	main()

